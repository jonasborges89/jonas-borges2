import axios from 'axios';

export default axios.create({
    baseURL : 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 8e2afe7e65a1b7a40dcea2d327cf7e18992f7034624da83d4958b57faab7984c'
    }
});

