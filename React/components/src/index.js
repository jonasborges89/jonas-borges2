import React from 'react';
import ReactDOM from 'react-dom';
import CommentDetail from './CommentDetail';
import faker from 'faker';
import ApprovalCard from './ApprovalCard';

const App = (props) => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <h4>
                    Warning!
                </h4>
                <p>Are you sure you want to continue?</p>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Sam" timeAgo="yesterday at 10:00 PM" avatar={faker.image.avatar()} comment="Some comment 1" />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Alex" timeAgo="yesterday at 07:52 AM" avatar={faker.image.avatar()} comment="Some comment 2" />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Jane" timeAgo="today at 09:00 PM" avatar={faker.image.avatar()} comment="Some comment 3" />
            </ApprovalCard>
        </div>
    )
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
)